<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>SDU</title>
	<!--Телефон арқылы кірген кезде сәйкес мөлшер болу үшін-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!--Bootstrap-->
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
	
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="project.css">
	
	 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script src="https://api-maps.yandex.ru/2.1/?apikey=Your API key&lang=en_US" type="text/javascript">
    </script>
    <script type="text/javascript">
        ymaps.ready(init);
        function init(){ 
            var myMap = new ymaps.Map("map", {
                center: [43.207717, 76.671361],
                zoom: 18.5
            });
        }


    </script>
</head>
<body>

<!-----------------------------NavBar--------------------------------->
	<nav class="navbar navbar-expand-md navbar-inverse bg-light sticky-top" >
		<a class="navbar-brand"><img src="Sdulife.png" width="50px"></a> 

  		<ul class="navbar-nav">
		    <li class="nav-item dropdown">
		      <a class="nav-link dropdown-toggle" data-toggle="dropdown" data-target="dropdown_target" href="#">Іс-шара</a>
		      <div class="dropdown-menu" aria-labelledby="dropdown_target">
		      	<a class="dropdown-item">Welcome Party</a>
		      	<a class="dropdown-item">SDU Heroes</a>
		      	<a class="dropdown-item">Nauryz</a>
		      	<a class="dropdown-item">Жаңа тыныс</a>
		      	<a class="dropdown-item">Photohunter</a>
		      	<a class="dropdown-item">SDU's Got Talent </a>
		      	<a class="dropdown-item">Football league</a>
		      </div>
		    </li>
		    
		    <li class="nav-item dropdown">
		      <a class="nav-link dropdown-toggle" data-toggle="dropdown" data-target="dropdown_target" href="#">Клубтар</a>
		      <div class="dropdown-menu" aria-labelledby="dropdown_target">
		      	<a class="dropdown-item" href="debate.html">Kazakh Debate Club</a>
		      	<a class="dropdown-item">Event Club</a>
		      	<a class="dropdown-item">Mountain Kings</a>
		      	<a class="dropdown-item">Zhasa Club</a>
		      	<a class="dropdown-item">Language Club</a>
		      	<a class="dropdown-item">Art Club</a>
		      	<a class="dropdown-item">Enactus Club</a>
		      	<a class="dropdown-item">Magzhan Club</a>
		      	<a class="dropdown-item" href="music.html">Music Club</a>
		      	<a class="dropdown-item">Shapagat Club</a>
		      	<a class="dropdown-item">Sport Club</a>
		      	<a class="dropdown-item">MMDance Club</a>
		      </div>
		    </li>
		    <li class="nav-item dropdown">
		      <a class="nav-link dropdown-toggle" data-toggle="dropdown" data-target="dropdown_target" href="#">Белсенділер</a>
		      <div class="dropdown-menu" aria-labelledby="dropdown_target">
		      	<a class="dropdown-item">SDULife құрамасы</a>
		      	<a class="dropdown-item">Клуб хэдтері</a>
		      	<a class="dropdown-item">Клуб мүшелері</a>
		      </div>
		    </li>

		    <li class="nav-item">
		      <a class="nav-link" href="#">Жаңалықтар</a>
		    </li>
		                     
  			</ul>
			</nav>
<!-----------------------------NavBar--------------------------------->
	<div class="ortasuret">
		<img src="sduphoto.jpeg" width="1000">
	</div>
	<script>
			


			$(document).ready(function(){
		  $("#bt2").click(function(){
		    $(".othernews").fadeTo("slow",1);
		  });
		});

			$(document).ready(function(){
		  $("#calendar").click(function(){
		    $("#calendar").animate({
		      left: '250px',
		      opacity: '1',
		      height: '300px',
		      width: '300px'
		    });
		  });
		});

			$(document).ready(function(){
		  $("#flip").click(function(){
		    $("#panel").slideDown(7000);
		  });
		  $("#stop").click(function(){
		    $("#panel").stop();
		  });
		});
	</script>

	
	<div style="display: flex; margin-left: 10px;">
		<img src="https://static.tildacdn.com/tild6131-3864-4437-b032-646631383366/2019_02_13_as_1-236.jpg" width="500">
		<h3>SDU life - бұл Сулеймен Демирель Университетінің студенттерінің оқудан тыс іс-шараларын ұйымдастырушы сайт</h3>
		<div>
			<br><br><br>
			<img id="calendar" src="calen.jpeg" width="300px">
		</div>
	</div>
	<br><br>
	<div class="hidefun">
			<div>
				<h3>Жаңалықтар</h3>
				<img src="welcomeparty.jpg" width="200px"> SDU да 1-інші курс студенттеріне арналған Welcome Party мерекесі аталып өтті.<a href="sdu.edu.kz"> Толығырақ</a> Бұл жаңалықты көрсетпеу.
			</div>

			<p><button id="bt2">Басқа да жаңалықтар</button></p>
			<div class="othernews" style="display: none; margin-bottom: 10px;">
				<img src="news2.png" width="200px"> Қарашаның 1-3 жұлдыз аралығында "Jaxart 2019" атты хакатон өтпекші<a href="sdu.edu.kz"> Толығырақ</a>
			</div>
		</div>
		<h1 style="margin-left: 500px;">About SDU</h1>
	<div style="margin-left: 20px;display: flex;">
		
		<video id="video" width="400px" height="400px" loop controls muted>
			<source src="SDU 45 SEK.mp4" type="video/mp4">
			Your browser does not support the video tag.
		</video>
		
		<div id="map" style="width: 600px; height: 300px; margin-left: 100px;margin-top: 50px"></div>

	</div>


	<br>
		
	<div style="display: flex;">
		<svg width="250" height="180">
  			<rect x="50" y="20" rx="20" ry="20" width="150" height="150"
  			style="fill:white;stroke:black;stroke-width:5;opacity:0.7" />
  			<line x1="60" y1="70" x2="190" y2="70" style="stroke:black;stroke-width:2;opacity:0.7" />
  			<line x1="60" y1="80" x2="190" y2="80" style="stroke:black;stroke-width:2;opacity:0.7" />
  			<line x1="60" y1="90" x2="190" y2="90" style="stroke:black;stroke-width:2;opacity:0.7" />
  			<line x1="60" y1="100" x2="190" y2="100" style="stroke:black;stroke-width:2;opacity:0.7" />
  			<line x1="60" y1="60" x2="190" y2="60" style="stroke:black;stroke-width:2;opacity:0.7" />
  			<line x1="60" y1="110" x2="190" y2="110" style="stroke:black;stroke-width:2;opacity:0.7" />
  			<line x1="60" y1="120" x2="190" y2="120" style="stroke:black;stroke-width:2;opacity:0.7" />
  			<line x1="60" y1="50" x2="190" y2="50" style="stroke:black;stroke-width:2;opacity:0.7" />
  			<line x1="60" y1="130" x2="190" y2="130" style="stroke:black;stroke-width:2;opacity:0.7" />
  			<line x1="60" y1="140" x2="190" y2="140" style="stroke:black;stroke-width:2;opacity:0.7" />
		</svg>
		<a href="bail.html">Кері байланыс</a>
	</div>

		<div id="p5">
		<h2> Contacts </h2>
		<div id="p51">
			<a href="https://www.instagram.com/alisher.erbolatuly/" > <img class="i5" src="https://m.buro247.ru/images/2018/02/nina_r/in_sta_main.jpg" target="_blank"> </a>
			<a href="mailto:180103299@stu.sdu.edu.kz" > <img class="i5" src="https://img2.freepng.ru/20180404/pge/kisspng-gmail-web-development-computer-icons-email-g-suite-gmail-5ac4902b2ae407.8856309615228314031757.jpg" target="_blank"> </a>
		</div>
		<p>+7 (778) 188-29-58</p>
		<p>© 2019 Alisher Duzmagambetov.</p>
	</div>
	
</body>
</html>